#! /usr/bin/env python
import os, sys
from pylab import legend, plot, loglog, show, title, xlabel, ylabel
import numpy as np

# PART I


#used lu_script to get these numbers with -mms2
N = np.array([49,169,625,2401,9409])
el2 = np.array([0.00341894,0.00030817,3.88389e-05,1.94412e-06,6.33007e-07])
einf = np.array([0.099418,0.0169964,0.0039253,0.000527934,0.000367842])


loglog(N, el2, 'r', N, 0.9*N **-1.5, 'r--', N, einf, 'g',N,0.9*1/N,'g--')
title('SNES modified Bratu MMS $u=\sin(\pi x) \sin(\pi y)$')
xlabel('Number of Dof $N$')
ylabel('Solution Error $e$')
legend(['$\ell_2$','$h^{-3}=N^{-3/2}$','$\ell_\infty$','$h^{-2}=N^{-1}$'],'upper right')
show() 




#PART II


# using MMS 2, results generated from lu_script1 and grid_sequence_script2 increasing the grid_sequencing
flops_GMRES = np.array([5.130e+06,3.89e+07,4.761e+08,7.063e+09])
e_GMRES = np.array([0.0246614,0.0169562,.0100672,0.00558216])


flops_LU = np.array([2.479e+06,2.450e+07,2.369e+08,3.463e+09])
e_LU = np.array([0.00030817,3.88389e-05,4.97907e-06,6.33007e-07])




loglog(flops_GMRES, e_GMRES, 'b', flops_LU, e_LU,'g')
title('Work-Precision with Grid Sequencing')
ylabel('Precision ($\ell_2$ error)')
xlabel('Work (Flops)')
legend(['GMRES', 'LU'])
show()

sys.exit('Take a break')



