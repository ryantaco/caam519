#! /usr/bin/env python
import os

sizes = []

times_ANDERSON = []
times_NEWTON = []


for k in range(4):
	Nx = 10 * 2**k
	modname = 'perfNEWTON%d' % k
	options_NEWTON = ['snes_type newtonls','-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-log_view',':%s.py:ascii_info_detail' % modname]
	os.system('./bin/problem3 '+ ' '.join(options_NEWTON))
	perfmod = __import__(modname)
	times_NEWTON.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])


for k in range(4):
	Nx = 10 * 2**k
	modname = 'perfANDERSON%d' % k
	options_ANDERSON = ['-snes_max_it 1000000', '-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-log_view',':%s.py:ascii_info_detail' % modname]
	os.system('./bin/problem3 '+ ' '.join(options_ANDERSON))
	perfmod = __import__(modname)
	sizes.append(Nx ** 2)
	times_ANDERSON.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])




print zip(sizes, times_ANDERSON, times_NEWTON)

from pylab import legend, plot, loglog, show, title, xlabel, ylabel

plot(times_ANDERSON, sizes, times_NEWTON, sizes)
title('Work-Precision')
xlabel('Time (s)')
ylabel('Problem Size $N$')
legend(['Anderson', 'Newton'])
show()

loglog(times_ANDERSON, sizes,  times_NEWTON, sizes)
title('Work-Precision loglog')
ylabel('Problem Size $N$')
xlabel('Time (s)')
legend(['Anderson', 'Newton'])
show()



#NOTE INITIAL RESULTS WITH NO ACTUAL FUNCTION LOOK LIKE:
#[(100, 0.000272036), (400, 0.000159979), (1600, 0.000128984), (6400, 0.000267982), (25600, 0.000233889)]

