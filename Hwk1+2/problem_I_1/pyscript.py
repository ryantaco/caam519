#! /usr/bin/env python
import os

solvername = 'KSPSolve'
sizes = []
times_ILU = []
times_GAMG = []
for k in range(5):
	Nx = 10 * 2**k
	modname = 'perfGAMG%d' % k
	options_GAMG = ['-pc_type gamg ', '-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-log_view',':%s.py:ascii_info_detail' % modname]
	os.system('./bin/ex5 '+ ' '.join(options_GAMG))
	perfmod = __import__(modname)
	sizes.append(Nx ** 2)
	times_GAMG.append(perfmod.Stages['Main Stage'][solvername][0]['time'])

for k in range(5):
	Nx = 10 * 2**k
	modname = 'perfILU%d' % k
	options_ILU = ['-pc_type ilu ', '-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-log_view',':%s.py:ascii_info_detail' % modname]
	os.system('./bin/ex5 '+ ' '.join(options_ILU))
	perfmod = __import__(modname)
	times_ILU.append(perfmod.Stages['Main Stage'][solvername][0]['time'])


print zip(sizes, times_ILU, times_GAMG)

# Determine regression slopes for the lines

from scipy import stats
import numpy as np

#linear slope
slopeILU = stats.linregress((sizes),(times_ILU))

slopeGAMG = stats.linregress((sizes),(times_GAMG))

print('Linear Slope ILU ' + str(slopeILU[0]))
print('Linear Slope GAMG '+ str(slopeGAMG[0]))

#log log slope
slopeILU = stats.linregress(np.log(sizes),np.log(times_ILU))

slopeGAMG = stats.linregress(np.log(sizes),np.log(times_GAMG))

print('Loglog Slope ILU ' + str(slopeILU[0]))
print('Loglog Slope GAMG '+ str(slopeGAMG[0]))

from pylab import legend, plot, loglog, show, title, xlabel, ylabel

plot(sizes, times_ILU, sizes, times_GAMG)
title('ex5')
xlabel('Problem Size $N$')
ylabel('Time (s)')
legend(['GMRES/ILU', 'GMRES/GAMG'])
show()

loglog(sizes, times_ILU, sizes, times_GAMG)
title('ex5 loglog')
xlabel('Problem Size $N$')
ylabel('Time (s)')
legend(['GMRES/ILU', 'GMRES/GAMG'])
show()

